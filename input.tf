variable "account" {
  description = "The AWS account number ID"
}

variable "bucket_name" {
  description = "The bucket name, ensure its globally unique"
  type = "string"
}

variable "acl" {
  description = "The S3 bucket 'Canned ACL' (https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl)"
  type = "string"
  default = "private"
}

variable "force_destroy" {
  description = "If the bucket must destroy all objects inside the bucket before delete the bucket (default: false)"
  type = "string"
  default = false
}

variable "versioning_enabled" {
  description = "If this bucket must versioning the objects. (default: false)"
  type = "string"
  default = false
}

variable "lifecycle_rule_id" {
  description = "The default name for the embbed lifecycle created by this module. (default: default-lifecycle)"
  type = "string"
  default = "default-lifecycle"
}

variable "lifecycle_rule_prefix" {
  description = "The key prefix inside S3 to apply the rule. (default: empty, apply to all bucket)"
  type = "string"
  default = ""
}

variable "lifecycle_rule_enabled" {
  description = "If the embbed rule is enabled or not. (default: false)"
  type = "string"
  default = false
}

variable "lifecycle_rule_days_sia" {
  description = "How many days the objects will stay in default storage area before go to SIA. (default: 30 days)"
  type = "string"
  default = 30
}

variable "lifecycle_rule_noncurrent_days_sia" {
  description = "How many days the objects older versions will stay in default storage area before go to SIA. (default: 30 days)"
  type = "string"
  default = 30
}

variable "lifecycle_rule_days_glacier" {
  description = "How many days the objects will stay in SIA storeage area before go to Glacier. (default: 90 days)"
  type = "string"
  default = 90
}

variable "lifecycle_rule_noncurrent_days_glacier" {
  description = "How many days the objects older versions will stay in SIA storeage area before go to Glacier. (default: 90 days)"
  type = "string"
  default = 90
}

variable "lifecycle_rule_expiration" {
  description = "In how many days the objects will be deleted. (default: 365 days)"
  type = "string"
  default = 365
}

variable "lifecycle_rule_noncurrent_expiration" {
  description = "In how many days the objects older versions will be deleted. (default: 365 days)"
  type = "string"
  default = 365
}

variable "policy" {
  description = "A valid bucket policy JSON document."
  default = ""
}

variable "enable" {
  default = true
}
